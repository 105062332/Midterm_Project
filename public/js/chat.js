
function init() {

    post_btn = document.getElementById('btn');
    post_txt = document.getElementById('content');
    back_to = document.getElementById('back');

    var user_email;
    var receive;
    var path;
    var first=0;
    var second=0;

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          user_email = user.email;
          var chat_num = window.location.href;
          var n = chat_num.indexOf('?');
          path = chat_num.substring(n+1);
          var str1 = chat_num.substring(n+1, chat_num.lastIndexOf('&'));
          var str2 = chat_num.substring(n+2+str1.length);
          var showf = firebase.database().ref("naming");

          showf.once('value')
          .then(function(snapshot){
            if(str1===user_email.substring(0,user_email.lastIndexOf('@')))
            {
                receive = str2;
                first = 1;
            }
            else if(str2==user_email.substring(0,user_email.lastIndexOf('@')))
            {
                receive = str1;
                first = 1;
            }
            else
            {
                receive = str1;
                first = 0;
            }
            snapshot.forEach(function(childSnapshot){
                var childData = childSnapshot.val();
                if(childData.UID.substring(0, childData.UID.lastIndexOf('@'))===receive)
                {
                    receive = childData.UID;
                    second = 1;
                }
            })
          })
          .then(function(){
            if(first===1 && second===1)
            {
                post_btn.addEventListener('click', function () {
                    var date = new Date();
                    var h = date.getHours();
                    var m = date.getMinutes();
                    var s = date.getSeconds();
                    if(h<10){
                        h = '0'+h;
                    }
                    if(m<10){
                        m = '0' + m;
                    }
                    if(s<10){
                        s = '0' + s;
                    }
            
                    var now = h+':'+m+':'+s;
            
                    if (post_txt.value != "") {
                        var content_len = post_txt.value;
                            if(content_len.length>=66)
                            {
                                var postData = {
                                    sender: user_email,
                                    receiver: receive,
                                    content: post_txt.value.slice(0,60),
                                    time : now,
                                };
                                console.log(post_txt.value.slice(0,60));

                            }
                            else
                            {
                                var postData = {
                                    sender: user_email,
                                    receiver: receive,
                                    content: post_txt.value,
                                    time : now,
                                };
                                console.log('1');
                            }
                            firebase.database().ref('chatting/'+path).push(postData);
                            post_txt.value = "";
                    }
                });
            
                window.addEventListener('keypress', function(event){
                    if(event.keyCode==13){
                        var date = new Date();
                        var h = date.getHours();
                        var m = date.getMinutes();
                        var s = date.getSeconds();
                        if(h<10){
                            h = '0'+h;
                        }
                        if(m<10){
                            m = '0' + m;
                        }
                        if(s<10){
                            s = '0' + s;
                        }
            
                        var now = h+':'+m+':'+s;
                        if (post_txt.value != "") {
                            var content_len = post_txt.value;
                            if(content_len.length>=66)
                            {
                                var postData = {
                                    sender: user_email,
                                    receiver: receive,
                                    content: post_txt.value.slice(0,60),
                                    time : now,
                                };
                            }
                            else
                            {
                                var postData = {
                                    sender: user_email,
                                    receiver: receive,
                                    content: post_txt.value,
                                    time : now,
                                };
                            }
                            firebase.database().ref('chatting/'+path).push(postData);
                            post_txt.value = "";
                        }
                    }
                })
            }
            else
            {
                alert("It's not a chat between two users!!");
                window.location = 'index.html';
            }
        })

        var postsRef = firebase.database().ref('chatting/'+path);
        var total_post = [];
        var first_count = 0;
        var second_count = 0;
          postsRef.once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (childSnapshot){
                    var childData = childSnapshot.val();
                    if(childData.sender == user_email)
                    {
                        total_post[total_post.length] = '<div class="now_acc">'+childData.sender+' 說</div><br><div class="content">'+childData.content+'</div><br>';
                    }
                    else
                    {
                        total_post[total_post.length] ='<div class="name">'+childData.sender+' 說</div><br><div class="content">'+childData.content+'</div><br>';
                    }
                    first_count += 1;
                });
                document.getElementById('show').innerHTML = total_post.join("");

                postsRef.on('child_added', function(data){
                    second_count+=1;
                    if(second_count>first_count){
                        var childData = data.val();
                        if(childData.sender == user_email)
                        {
                            total_post[total_post.length] = '<div class="now_acc">'+childData.sender+' 說</div><br><div class="content">'+childData.content+'</div><br>';
                        }
                        else
                        {
                            total_post[total_post.length] ='<div class="name">'+childData.sender+' 說</div><br><div class="content">'+childData.content+'</div><br>';
                        }
                        document.getElementById('show').innerHTML = total_post.join('');
                    }
                    $('#show').scrollTop($('#show')[0].scrollHeight);
                })

            })
            .catch(e => console.log(e.message));

        } else {
          console.log("nouser");
        }
      });
    

    back_to.addEventListener('click',function(){
        window.location = 'index.html';
    })

}

/*var profile_pic = document.getElementById('pic');

profile_pic.addEventListener('change',function(){
    var oFReader = new FileReader();
    var file = profile_pic.files[0];
    oFReader.readAsDataURL(file);
    oFReader.onload = function (oFREvent) {
        ProfileImage.src = oFREvent.target.result;
    };

    const metadata = {
        contentType: file.type
    };

    firebase.storage().ref("User_list/" + profile_ID + "/user_image").put(file,metadata).then((snapshot) => {
        const url = snapshot.downloadURL;
        firebase.database().ref("User_list/" + profile_ID + "/imageURL").set(url);
        
    })
})*/

window.onload = function () {
    init();
};