
function init() {

    post_btn = document.getElementById('btn');
    post_txt = document.getElementById('content');
    back_to = document.getElementById('back');

    var user_email;
    var receive;
    var path;

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          user_email = user.email;
          var chat_num = window.location.href;
          var n = chat_num.indexOf('?');
          path = chat_num.substring(n+1);
          var showg = firebase.database().ref("group_chatting/"+path);
          var grouping = firebase.database().ref("grouping");
          var real = 0;
          var total_post = [];
          var first_count = 0;
          var second_count = 0;
          showg.once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (childSnapshot){
                    var childData = childSnapshot.val();
                    if(childData.sender == user_email)
                    {
                        total_post[total_post.length] = '<div class="now_acc">'+childData.sender+' 說</div><br><div class="content">'+childData.content+'</div><br>';
                    }
                    else
                    {
                        total_post[total_post.length] ='<div class="name">'+childData.sender+' 說</div><br><div class="content">'+childData.content+'</div><br>';
                    }
                    first_count += 1;
                });
                document.getElementById('show').innerHTML = total_post.join("");

                showg.on('child_added', function(data){
                    second_count+=1;
                    if(second_count>first_count){
                        var childData = data.val();
                        if(childData.sender == user_email)
                        {
                            total_post[total_post.length] = '<div class="now_acc">'+childData.sender+' 說</div><br><div class="content">'+childData.content+'</div><br>';
                        }
                        else
                        {
                            total_post[total_post.length] ='<div class="name">'+childData.sender+' 說</div><br><div class="content">'+childData.content+'</div><br>';
                        }
                        document.getElementById('show').innerHTML = total_post.join('');
                    }
                    $('#show').scrollTop($('#show')[0].scrollHeight);
                })

            })
            .catch(e => console.log(e.message))
            .then(function(){
                grouping.once('value')
                    .then(function(snapshot){
                        snapshot.forEach(function(childSnapshot){
                            var childData = childSnapshot.val();
                            if(childData.group_name===path)
                            {
                                real = 1;
                            }
                        })
                    })
                    .then(function(){
                        if(real!==1)
                        {
                            alert('Create this group first!!');
                            window.location = 'index.html'
                        }
                    })
            })

        } else {
          console.log("nouser");
        }
    });

    post_btn.addEventListener('click', function () {
        var date = new Date();
        var h = date.getHours();
        var m = date.getMinutes();
        var s = date.getSeconds();
        if(h<10){
            h = '0'+h;
        }
        if(m<10){
            m = '0' + m;
        }
        if(s<10){
            s = '0' + s;
        }

        var now = h+':'+m+':'+s;

        if (post_txt.value != "") {
            if(post_txt.value.length>=60)
            {
                var postData = {
                    sender: user_email,
                    content: post_txt.value.slice(0,60),
                    time : now,
                };
            }
            else
            {
                var postData = {
                    sender: user_email,
                    content: post_txt.value,
                    time : now,
                };
            }
            firebase.database().ref('group_chatting/'+path).push(postData);
            post_txt.value = "";
        }
    });

    window.addEventListener('keypress', function(event){
        if(event.keyCode==13){
            var date = new Date();
            var h = date.getHours();
            var m = date.getMinutes();
            var s = date.getSeconds();
            if(h<10){
                h = '0'+h;
            }
            if(m<10){
                m = '0' + m;
            }
            if(s<10){
                s = '0' + s;
            }

            var now = h+':'+m+':'+s;

            if (post_txt.value != "") {
                if(post_txt.value.length>=60)
                {
                    var postData = {
                        sender: user_email,
                        content: post_txt.value.slice(0,60),
                        time : now,
                    };
                }
                else
                {
                    var postData = {
                        sender: user_email,
                        content: post_txt.value,
                        time : now,
                    };
                }
                firebase.database().ref('group_chatting/'+path).push(postData);
                post_txt.value = "";
            }
        }
    })

    back_to.addEventListener('click',function(){
        window.location = 'index.html';
    })

}


window.onload = function () {
    init();
};