var user_recog =[];
var user_email = '';

function init() {
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var friends = document.getElementById('dynamic-menu2');
        var groups = document.getElementById('dynamic-menu3');
        var showf = firebase.database().ref("naming");
        var showg = firebase.database().ref("grouping");
        var total_user = [];
        var total_group =[];
        var second=0;
        var count = 0;
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>"+"</span><span class='dropdown-item' id='newPassword'>Change Password</span>";
            
            // logout
            var logout = document.getElementById('logout-btn');
            logout.addEventListener('click', function(){
                firebase.auth().signOut()
                    .then(function(){
                        alert('logout sucess!!');
                    })
                    .catch(function(error){
                        alert('Error!');
                    })
            });
            
            newPassword.addEventListener('click', function() {
                loginUser = firebase.auth().currentUser;
                console.log(loginUser.email);
                alert("Please check out your email to change password!");
                firebase.auth().sendPasswordResetEmail(loginUser.email).then(function() {
                    //loginUser.updatePassword(newPassword.value).then(function() {
                        alert("Email Sent!");
                    }).catch(function(error) {
                        alert("change error !!!!!!!!!!!");
                    });
                })
                

            // show the group that the user have now
            showg.once('value')
                .then(function(snapshot){
                    snapshot.forEach(function(childSnapshot){
                        var childText = childSnapshot.val();
                        var sum = 0;
                        var members;
                        var counted=-1;
                        var string1 = childText.member;
                        var substr ="";
                        for(sum=0 ; sum <childText.sum; sum++)
                        {
                            substr = string1.substring(0, string1.lastIndexOf('&'));
                            counted = substr.length;
                            if(sum!==childText.sum-1)
                                members = string1.substring(counted+1);
                            else
                                members = string1;
                            string1 = substr;
                            

                            if(members===user_email)
                            {
                                total_group[total_group.length] = "<a class='dropdown-item' href = 'group_chat.html?"+childText.group_name+"'>"+childText.group_name;
                            }
                            //console.log(counted);
                        }
                        groups.innerHTML = total_group.join('');
                    })
                });

            // show the list of all user in this website: friends
            showf.once('value')
                .then(function(snapshot){
                    snapshot.forEach(function(childSnapshot){
                        var childData = childSnapshot.val();
                        if(childData.UID!==user_email){
                            total_user[total_user.length] = "<a class='dropdown-item recognize' value='"+childData.UID+"'>"+childData.UID;
                            user_recog[second] = childData.UID;
                            second+=1;
                        }
                        else{
                            count++;
                        }
                        friends.innerHTML = total_user.join('');
                    })
                })
                .catch(console.log("TT"))
                .then(function(){
                    total_group[total_group.length] = "<a class='dropdown-item' id='create_group'>Create Group";
                    groups.innerHTML = total_group.join('');

                    var creating = document.getElementById('create_group');
                    creating.addEventListener('click',function(){
                        window.location = 'create_group.html';
                    });

                    if(count===0)
                    {
                        var users = {
                            UID: user_email
                        };
                        firebase.database().ref("naming").push(users);
                    }
                    
                    count = 0;
                    second = 0;
                })
                .then(function(){
                    var target = document.querySelectorAll(".recognize");
                    for(var i=0; i<target.length; i++)
                    {
                        target[i].onclick = (function(number){
                            return function(){
                                var str = user_email.substring(0, user_email.lastIndexOf('@'));
                                var str2 = [];
                                str2[number] = user_recog[number].substring(0, user_recog[number].lastIndexOf('@'));
                                var Ref = firebase.database().ref('chatting');
                                Ref.once('value')
                                    .then(function(snapshot){
                                        if(!snapshot.hasChild(str + "&" + str2[number]) && !snapshot.hasChild(str2[number] + "&" + str))
                                        {
                                            window.location = 'chat.html?'+str+"&"+str2[number];
                                        }
                                        else if(snapshot.hasChild(str+"&"+str2[number])){
                                            window.location = 'chat.html?'+str+'&'+str2[number];
                                        }
                                        else if(snapshot.hasChild(str2[number]+'&'+str)){
                                            window.location = 'chat.html?'+str2[number]+'&'+str;
                                        }
                                        
                                    })
                            }
                        })(i);
                    }
                })
                .then(function(){
                    var online = firebase.database().ref('chatting');
                    var str=[];
                    var s = 0;

                    online.once('value')
                        .then(function(snapshot){
                            for(var i=0;i<user_recog.length; i++)
                            {
                                var str1 = user_email.substring(0, user_email.lastIndexOf('@'));
                                var str2 = user_recog[i].substring(0, user_recog[i].lastIndexOf('@'));
                                if(snapshot.hasChild(str2+'&'+str1) || snapshot.hasChild(str1+'&'+str2))
                                {
                                    str[s] = str2;
                                    s++;
                                }
                            }

                            if (!("Notification" in window)) {
                                alert("This browser does not support desktop notification");
                            }
                            
                                // Let's check whether notification permissions have already been granted
                            else if (Notification.permission === "granted") {
                             // If it's okay let's create a notification
                                for(var j=0; j<s;j++)
                                {
                                    var notification = new Notification("You are now chatting with: "+str[j]);
                                }
                            }
                            
                            // Otherwise, we need to ask the user for permission
                            else if (Notification.permission !== "denied") {
                                Notification.requestPermission(function (permission) {
                                    // If the user accepts, let's create a notification
                                if (permission === "granted") {
                                    for(var j=0; j<s;j++)
                                    {
                                        var notification = new Notification("You are now chatting with: "+str[j]);
                                    }
                                }
                                });
                            }
                        });
                });
            //showf.push(user_email);
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            friends.innerHTML = "<span class='dropdown-item'>"
            groups.innerHTML = "<span class='dropdown-item'>"
        }
    });
}

window.onload = function () {
    init();
};