var user_recog = [];

function init(){
    firebase.auth().onAuthStateChanged(function(user){
        if(user){
            var showf = firebase.database().ref("naming");
            var total_user=[];
            var user_email=user.email;
            var second=0;
            var groups = document.getElementById('list');
            var type;

            showf.once('value')
            .then(function(snapshot){
                snapshot.forEach(function(childSnapshot){
                    var childData = childSnapshot.val();
                    if(childData.UID!==user_email){
                        total_user[total_user.length] = "<button class='list-group-item list-group-item-action member' value='"+childData.UID+"'>"+childData.UID;
                        user_recog[second] = childData.UID;
                        second+=1;
                    }
                    groups.innerHTML = total_user.join('');
                })
            })
            .catch(console.log("what!!"))
            .then(function(){
                var select = document.querySelectorAll('.member');
                
                for(var i=0;i<select.length;i++){
                    select[i].onclick = (function(number){
                        return function(){
                            select[number].classList.toggle('active');
                        }
                    })(i);
                }
            })
            .then(function(){
                var create = document.getElementById('btnCreate');
                var Name = document.getElementById('groupName');
                var can_not = 0;

                create.addEventListener('click', function(){
                    var allMem = document.querySelectorAll('.active');
                    can_not = 0;
                    if(allMem.length<=1)
                        alert('Please choose at least two members');
                    else{
                        var str1 = user_email+'&';

                        for(var j=0;j<allMem.length-1;j++)
                        {
                            str1 = str1+allMem[j].value+"&";
                        }
                        str1 = str1+allMem[j].value;

                        firebase.database().ref('grouping').once('value')
                            .then(function(snapshot){
                                snapshot.forEach(function(childSnapshot){
                                    var childData = childSnapshot.val();
                                    if(childData.group_name===Name.value)
                                    {
                                        console.log(can_not);
                                        can_not = 1;
                                    }
                                    else if(Name.value==="")
                                    {
                                        can_not = 2;
                                    }
                                });
                            })
                            .then(function(){
                                if(can_not===1)
                                {
                                    alert('The group name has been used!');
                                }
                                else if(can_not===2)
                                {
                                    alert('You have to enter a name!');
                                }
                                else{
                                    var grouping = firebase.database().ref('grouping');

                                    var data = {
                                        member: str1,
                                        sum: allMem.length+1,
                                        group_name: Name.value
                                    };

                                    grouping.push(data);
                                    can_not = 0;
                                    window.location = 'group_chat.html?'+Name.value;

                                }
                            });
                    }
                });
            })
        }
    })
}

window.onload = function(){
    init();
}