# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Chatting room
* Key functions (add/delete)
    1. chat 1 on 1
    2. load message history
    3. chat with new user
    4. 
* Other functions (add/delete)
    1. create a chatting group
    2. a public chatting room for all users
    3. after a chat, it will show who you are talking with at index page
    4. change password

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|N|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
所有的功能操作都與上方的Nav bar 有關，在登陸前點開Account會看到Login，登入後點開Account可以看見Logout和Change Password，點擊Logout登出，點擊Change Password則會寄送email至註冊信箱，點開信件連結後即可輸入新密碼。

一、 Nav Bar功能介紹
    1 on 1: 
        點開1 on 1以後可以看到其他的用戶，點擊想聊天的用戶就可以進入一對一的聊天室。
    Group: 
        點開Group會顯示當前使用者的所有群組，若沒有群組則只會顯示create group，點擊後進入建立群組的頁面；在該頁面選擇想建立群組的成員後輸入群    組名稱即可建立群組。
    Public: 
        Public則是提供一個公開的聊天室給所有使用者。

二、 聊天室
    在1對1的聊天室設計中，點擊聊天對像後會以跳轉到chat.html?user1&user2網址的形式進到聊天室裡，藉由網址分辨是誰在聊天，並把資料上傳到database；群組聊天室也是一樣的設計，只不過網址裡存的是群組名稱。
    在訊息傳送、顯示方面，有限制顯示的字數上限，以避免使用者打太多字導致網頁破版，而自己的名字會靠向畫面右邊，其他使用者的名稱會靠右邊，按下Enter也可以發送訊息，當訊息變多時會自動滑到最新的對話內容。

三、群組
    建立群組本身並不是一個困難的問題，然而我在如何判斷當前使用者有哪些群組時遇上了一些困難，我的作法是在創建群組時就將群組名稱、成員，和成員總數上傳到database，在index.html時裡用forEach函式，對每個群組的使用者進行搜尋，如果發現當前的使用者，則把改群組顯示在group選單內，然此作法在群組數多的情況下會花費不少時間；倘若能以group/userID的路徑下去儲存群組名稱，在顯示的時候就可以直接抓取該帳號下的所有群組，應是較為妥當的做法。

## Security Report (Optional)

一、 網址顯示使用者
    在1對1聊天室中因為是由網址去取得使用者名稱，並藉此上傳資料，倘若他人將網址改成不存在的使用者，則可能誤存了實際上不存在的使用者的資料；或者將使用者改成其他非本人的真實使用者以竊取聊天紀錄，故而進入聊天室時我會先檢查網址上是否都是真正、當前的使用者才能繼續聊天，否則就自動跳回首頁。

二、 資料讀寫
    所有的資料讀取與寫入都需要登入才能進行操作，避免非用戶竊讀資料。